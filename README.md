# Sentinel Mainnet on MANTRA DAO

- Chain ID: `sentinelhub-2`

## Submodules

- [sentinel-official/hub](https://github.com/sentinel-official/hub.git) @ `v0.9.2`
